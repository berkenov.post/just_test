class Contact {
  final String name;
  final String phone;
  final String city;

  Contact({required this.name, required this.phone, required this.city});
}

final data = [
  Contact(name: "Bob", phone: "+1 (566) 777 77 77", city: "New York"),
  Contact(name: "Alice", phone: "+1 (566) 777 77 77", city: "Los Angeles"),
  Contact(name: "Eve", phone: "+1 (566) 777 77 77", city: "San Francisco"),
  Contact(name: "Marry", phone: "+1 (566) 777 77 77", city: "San Diego"),
  Contact(name: "Kate", phone: "+1 (566) 777 77 77", city: "San Jose"),
  Contact(name: "Linda", phone: "+1 (566) 777 77 77", city: "Chicago"),
];
