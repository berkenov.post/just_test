import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppIcon extends StatelessWidget {
  final String name;
  final double? width;
  final double? height;
  final Color? color;

  const AppIcon(
    this.name, {
    Key? key,
    this.width,
    this.height,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/ico/$name.svg',
      width: width,
      height: height,
      color: color,
    );
  }
}
