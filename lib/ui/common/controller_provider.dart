import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class ControllerProvider<T extends ChangeNotifier> extends ChangeNotifierProvider<T> {
  ControllerProvider({
    Key? key,
    required T Function() create,
    required Widget Function(BuildContext, T) builder,
  }) : super(
          key: key,
          create: (_) => create(),
          child: Consumer<T>(
            builder: (context, controller, _) => builder(context, controller),
          ),
        );
}
