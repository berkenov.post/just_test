import 'package:flutter/material.dart';

abstract class AppColors {
  const AppColors._();

  static const Color backgroundColor = Color(0xFFF5F7FA);
  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color disabledTextColor = Color(0xFF8693A3);
  static const Color selectedColor = Color(0xFFBFFF07);
  static const Color textColor = Color(0xFF29364E);
}
