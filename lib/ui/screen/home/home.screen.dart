import 'package:flutter/material.dart';
import 'package:just_test/data/mock.dart';
import 'package:just_test/ui/common/controller_provider.dart';
import 'package:just_test/ui/screen/home/home.controller.dart';
import 'package:just_test/ui/screen/home/widget/filter/filter.widget.dart';
import 'package:just_test/ui/screen/home/widget/phones_cart.widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SingleChildScrollView(
        child: ControllerProvider(
          create: () => HomeController(),
          builder: (context, ctrl) {
            return Column(
              children: [
                Filter(ctrl: ctrl),
                const SizedBox(height: 14),
                PhonesCart(
                  contacts: data.take(3).toList(),
                ),
                PhonesCart(
                  contacts: data.take(2).toList(),
                ),
                PhonesCart(
                  contacts: data.take(4).toList(),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
