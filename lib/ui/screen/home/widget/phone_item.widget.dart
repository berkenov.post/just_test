import 'package:flutter/material.dart';
import 'package:just_test/data/mock.dart';
import 'package:just_test/ui/common/app_icon.widget.dart';
import 'package:just_test/ui/style/color.dart';

class PhoneItem extends StatelessWidget {
  final Contact contact;

  const PhoneItem({
    Key? key,
    required this.contact,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 12),
      child: ListTile(
        leading: const CircleAvatar(
          radius: 20,
          backgroundColor: AppColors.white,
          child: AppIcon('ic_phone'),
        ),
        title: Text(
          contact.phone,
          style: const TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w600,
            color: AppColors.textColor,
          ),
        ),
        subtitle: Text(contact.city,
            style: const TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w600,
              color: AppColors.disabledTextColor,
            )),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildItemControlButton(() {}, 'S'),
            const SizedBox(width: 8),
            _buildItemControlButton(() {}, 'V'),
          ],
        ),
      ),
    );
  }

  Widget _buildItemControlButton(VoidCallback onTap, String text) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(20),
      child: Container(
        height: 35,
        width: 35,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: AppColors.white,
        ),
        child: Text(text,
            style: const TextStyle(
              fontSize: 11,
              color: AppColors.textColor,
              fontWeight: FontWeight.w600,
            )),
      ),
    );
  }
}
