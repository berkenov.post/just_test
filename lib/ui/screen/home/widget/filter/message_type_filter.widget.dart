import 'package:flutter/material.dart';
import 'package:just_test/ui/screen/home/home.controller.dart';
import 'package:just_test/ui/style/color.dart';

class MessageTypeFilter extends StatelessWidget {
  final HomeController ctrl;

  const MessageTypeFilter({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        _buildFilterButton(EMessageType.sms),
        const SizedBox(width: 8),
        _buildFilterButton(EMessageType.mms),
        const SizedBox(width: 8),
        _buildFilterButton(EMessageType.voice),
      ],
    );
  }

  Widget _buildFilterButton(EMessageType type) {
    final isSelected = ctrl.messageType == type;
    return Expanded(
      child: GestureDetector(
        onTap: () {
          ctrl.setMessageType(type);
        },
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 16),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: isSelected ? AppColors.selectedColor : AppColors.white,
            borderRadius: BorderRadius.circular(100),
          ),
          child: Text(
            type.name,
            style: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w500,
              color: isSelected ? AppColors.black : AppColors.disabledTextColor,
            ),
          ),
        ),
      ),
    );
  }
}
