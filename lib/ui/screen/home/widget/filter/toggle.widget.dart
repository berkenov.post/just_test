import 'package:flutter/material.dart';
import 'package:just_test/ui/screen/home/home.controller.dart';
import 'package:just_test/ui/style/color.dart';

class Toggle extends StatelessWidget {
  final HomeController ctrl;

  const Toggle({Key? key, required this.ctrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isSelected = ctrl.showNumberWithoutVerified;
    return GestureDetector(
      onTap: ctrl.toggleShowNumberWithoutVerified,
      child: Row(
        children: [
          Container(
            width: 16,
            height: 16,
            decoration: BoxDecoration(
              color: isSelected ? AppColors.selectedColor : AppColors.white,
              borderRadius: BorderRadius.circular(16),
            ),
            child: isSelected
                ? const Icon(
                    Icons.check,
                    size: 12,
                    color: AppColors.black,
                  )
                : null,
          ),
          const SizedBox(width: 8),
          const Text('Show number without verification'),
        ],
      ),
    );
  }
}
