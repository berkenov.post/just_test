import 'package:flutter/material.dart';
import 'package:just_test/ui/screen/home/home.controller.dart';
import 'package:just_test/ui/style/color.dart';

class AppDropDown extends StatelessWidget {
  final HomeController ctrl;

  const AppDropDown({Key? key, required this.ctrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<EPhoneType>(
      value: ctrl.phoneType,
      onChanged: (EPhoneType? newValue) {
        if (newValue != null) {
          ctrl.setPhoneType(newValue);
        }
      },
      items: EPhoneType.values.map((EPhoneType value) {
        return DropdownMenuItem<EPhoneType>(
          value: value,
          child: Text(
            value.name,
            style: const TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w600,
              color: AppColors.black,
            ),
          ),
        );
      }).toList(),
      decoration: const InputDecoration(
        filled: true,
        fillColor: AppColors.white,
        labelStyle: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w600,
          color: AppColors.backgroundColor,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: AppColors.backgroundColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          borderSide: BorderSide(
            color: AppColors.backgroundColor,
          ),
        ),
        suffixIcon: Icon(
          Icons.keyboard_arrow_down,
          color: AppColors.black,
        ),
      ),
      icon: const SizedBox.shrink(),
    );
  }
}
