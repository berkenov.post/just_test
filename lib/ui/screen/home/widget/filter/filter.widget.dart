import 'package:flutter/material.dart';
import 'package:just_test/ui/screen/home/home.controller.dart';
import 'package:just_test/ui/screen/home/widget/filter/toggle.widget.dart';
import 'package:just_test/ui/style/color.dart';

import 'app_drop_down.widget.dart';
import 'message_type_filter.widget.dart';

class Filter extends StatelessWidget {
  final HomeController ctrl;

  const Filter({
    Key? key,
    required this.ctrl,
  }) : super(key: key);

  EMessageType get currentType => ctrl.messageType;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 17),
      decoration: BoxDecoration(
        color: AppColors.backgroundColor,
        borderRadius: BorderRadius.circular(18),
      ),
      child: Column(
        children: [
          MessageTypeFilter(ctrl: ctrl),
          const SizedBox(height: 16),
          AppDropDown(ctrl: ctrl),
          const SizedBox(height: 16),
          Toggle(ctrl: ctrl),
        ],
      ),
    );
  }
}
