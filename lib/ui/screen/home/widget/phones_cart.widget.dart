import 'package:flutter/material.dart';
import 'package:just_test/data/mock.dart';
import 'package:just_test/ui/screen/home/widget/phone_item.widget.dart';
import 'package:just_test/ui/style/color.dart';

class PhonesCart extends StatelessWidget {
  final List<Contact> contacts;

  const PhonesCart({
    Key? key,
    required this.contacts,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 16),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                height: 24,
                width: 24,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  image: const DecorationImage(
                    image: AssetImage('assets/country/img_usa.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(height: 16),
          Container(
            decoration: BoxDecoration(
              color: AppColors.backgroundColor,
              borderRadius: BorderRadius.circular(18),
            ),
            child: Column(
              children: contacts.map((contact) => PhoneItem(contact: contact)).toList(),
            ),
          ),
        ],
      ),
    );
  }
}
