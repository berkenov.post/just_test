import 'package:flutter/cupertino.dart';

enum EMessageType {
  sms,
  mms,
  voice,
}

extension FilterTypeExtension on EMessageType {
  String get name {
    switch (this) {
      case EMessageType.sms:
        return 'SMS';
      case EMessageType.mms:
        return 'MMS';
      case EMessageType.voice:
        return 'Voice';
    }
  }
}

enum EPhoneType {
  landlineOrMobile,
  landline,
  mobile,
  tollFree,
  unknown,
}

extension PhoneTypeExtension on EPhoneType {
  String get name {
    switch (this) {
      case EPhoneType.landlineOrMobile:
        return 'Landline or Mobile';
      case EPhoneType.landline:
        return 'Landline';
      case EPhoneType.mobile:
        return 'Mobile';
      case EPhoneType.tollFree:
        return 'Toll-Free';
      case EPhoneType.unknown:
        return 'Unknown';
    }
  }
}

class HomeController extends ChangeNotifier {
  EMessageType get messageType => _messageType;
  EMessageType _messageType = EMessageType.sms;

  void setMessageType(EMessageType type) {
    _messageType = type;
    notifyListeners();
  }

  EPhoneType get phoneType => _phoneType;
  EPhoneType _phoneType = EPhoneType.landlineOrMobile;

  void setPhoneType(EPhoneType type) {
    _phoneType = type;
    notifyListeners();
  }

  bool get showNumberWithoutVerified => _showNumberWithoutVerified;
  bool _showNumberWithoutVerified = true;

  void toggleShowNumberWithoutVerified() {
    _showNumberWithoutVerified = !_showNumberWithoutVerified;
    notifyListeners();
  }
}
