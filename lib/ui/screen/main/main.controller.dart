import 'package:flutter/cupertino.dart';

enum ETabState { home, phone, messages, contacts }

extension ETabStateExtension on ETabState {
  String get name {
    switch (this) {
      case ETabState.home:
        return 'Home';
      case ETabState.phone:
        return 'Phone';
      case ETabState.messages:
        return 'Messages';
      case ETabState.contacts:
        return 'Contacts';
    }
  }

  String get icon {
    switch (this) {
      case ETabState.home:
        return 'ic_home';
      case ETabState.phone:
        return 'ic_phone';
      case ETabState.messages:
        return 'ic_message';
      case ETabState.contacts:
        return 'ic_contact';
    }
  }
}

class MainController extends ChangeNotifier {
  ETabState get currentTab => _currentTab;
  ETabState _currentTab = ETabState.home;

  void onTabChange(ETabState tab) {
    _currentTab = tab;
    notifyListeners();
  }
}
