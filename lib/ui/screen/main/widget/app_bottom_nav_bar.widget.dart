import 'package:flutter/material.dart';
import 'package:just_test/ui/common/app_icon.widget.dart';
import 'package:just_test/ui/screen/main/main.controller.dart';
import 'package:just_test/ui/style/color.dart';

class AppBottomNavBar extends StatefulWidget {
  final ETabState currentTab;
  final void Function(ETabState) onTabChange;

  const AppBottomNavBar({
    Key? key,
    required this.currentTab,
    required this.onTabChange,
  }) : super(key: key);

  @override
  State<AppBottomNavBar> createState() => _AppBottomNavBarState();
}

class _AppBottomNavBarState extends State<AppBottomNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.backgroundColor,
      padding: const EdgeInsets.only(top: 22, bottom: 28),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildTabItem(ETabState.home),
          _buildTabItem(ETabState.phone),
          _buildTabItem(ETabState.messages),
          _buildTabItem(ETabState.contacts),
        ],
      ),
    );
  }

  Widget _buildTabItem(ETabState tabState) {
    final isSelected = widget.currentTab == tabState;
    return GestureDetector(
      onTap: () => widget.onTabChange(tabState),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        decoration: BoxDecoration(
          color: isSelected ? AppColors.selectedColor : null,
          borderRadius: BorderRadius.circular(100),
        ),
        child: isSelected ? _buildHorizontalTabItem(tabState) : _buildVerticalTabItem(tabState),
      ),
    );
  }

  Widget _buildVerticalTabItem(ETabState tabState) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        AppIcon(tabState.icon),
        const SizedBox(height: 4),
        Text(
          tabState.name,
          style: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w700,
            color: AppColors.disabledTextColor,
          ),
        ),
      ],
    );
  }

  Widget _buildHorizontalTabItem(ETabState tabState) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        AppIcon(
          tabState.icon,
          color: AppColors.black,
        ),
        const SizedBox(width: 4),
        Text(
          tabState.name,
          style: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w700,
            color: AppColors.black,
          ),
        ),
      ],
    );
  }
}
