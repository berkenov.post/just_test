import 'package:flutter/material.dart';
import 'package:just_test/ui/common/app_icon.widget.dart';
import 'package:just_test/ui/style/color.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildMenuButton(context),
            const SizedBox(width: 8),
            const Expanded(
              child: Text(
                '+12021234567',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: AppColors.black,
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            const SizedBox(width: 8),
            _buildButtons(() {}, 'ic_cart'),
            const SizedBox(width: 8),
            _buildButtons(() {}, 'ic_message'),
            const SizedBox(width: 8),
            _buildButtons(() {}, 'ic_bell'),
          ],
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(64);

  Widget _buildMenuButton(BuildContext context) {
    return GestureDetector(
      onTap: Scaffold.of(context).openDrawer,
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: AppColors.backgroundColor,
        ),
        child: const AppIcon(
          'ic_menu',
          width: 24,
          height: 24,
        ),
      ),
    );
  }

  Widget _buildButtons(Function() onTap, String icon) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: AppColors.backgroundColor,
        ),
        child: AppIcon(
          icon,
          color: AppColors.black,
        ),
      ),
    );
  }
}
