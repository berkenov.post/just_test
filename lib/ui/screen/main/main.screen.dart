import 'package:flutter/material.dart';
import 'package:just_test/ui/common/controller_provider.dart';
import 'package:just_test/ui/screen/calls/calls.screen.dart';
import 'package:just_test/ui/screen/contacts/contacts.screen.dart';
import 'package:just_test/ui/screen/home/home.screen.dart';
import 'package:just_test/ui/screen/main/widget/app_bar.widget.dart';
import 'package:just_test/ui/screen/main/widget/app_bottom_nav_bar.widget.dart';
import 'package:just_test/ui/screen/main/widget/drawer.screen.dart';
import 'package:just_test/ui/screen/messages/messages.screen.dart';
import 'package:just_test/ui/style/color.dart';

import 'main.controller.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final screens = [
    const HomeScreen(),
    const CallsScreen(),
    const MessagesScreen(),
    const ContactsScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return ControllerProvider<MainController>(
        create: () => MainController(),
        builder: (context, ctrl) {
          return Scaffold(
            backgroundColor: AppColors.white,
            appBar: const CustomAppBar(),
            body: SafeArea(
              child: screens[ctrl.currentTab.index],
            ),
            drawer: const DrawerScreen(),
            bottomNavigationBar: AppBottomNavBar(
              currentTab: ctrl.currentTab,
              onTabChange: ctrl.onTabChange,
            ),
          );
        });
  }
}
