import 'package:flutter/material.dart';
import 'package:just_test/ui/screen/main/main.screen.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Roboto',
      ),
      title: 'Call Manager',
      home: const MainScreen(),
    );
  }
}
